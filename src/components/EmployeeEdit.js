import _ from 'lodash';
import  React, { Component } from 'react';
import { View, Text, Modal, Alert} from 'react-native';
import { Card, CardSection, Button } from './common';
import { connect } from 'react-redux';
import Communications from 'react-native-communications';
import { employeeUpdate, employeeSave, employeeDelete } from '../actions';
import EmployeeForm from './EmployeeForm';

class EmployeeEdit extends Component {

  state = { showModel: false }

  onDeleteBTN = (uid) => {
    this.props.employeeDelete(uid)
  }

  ShowAlertDialog() {
   
  Alert.alert(
    'Employee Fire',
    'Are You sure to fire this employee',
    [
      {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      {text: 'OK', onPress: () => this.onDeleteBTN({uid: this.props.employee.uid })},
    ],
    { cancelable: false }
  )
}
  componentWillMount() {
    _.each(this.props.employee, (value, prop) =>{
      this.props.employeeUpdate({ prop, value });
    })
  }

  onButtonPress() {
    const { name, phone, shift} = this.props;
    this.props.employeeSave({name, phone, shift, uid: this.props.employee.uid})
  }

  onTextPress() {
    const { phone, shift} = this.props;
    Communications.text(phone, `your update Schedule is ${shift}`)
  }


  render() {
    return (
      <Card>
      <EmployeeForm />

      <CardSection>
        <Button onPress={this.onButtonPress.bind(this)}>
          Save Changes
        </Button>
      </CardSection>

      <CardSection>
        <Button onPress={this.onTextPress.bind(this)}>
          Text Schedule
        </Button>
      </CardSection>

      <CardSection>
        <Button onPress={this.ShowAlertDialog.bind(this)}>
          Fire Employee
        </Button>
      </CardSection>

      </Card>
    )
  }
}

const mapStateToProps = state => {
  const { name, phone, shift } = state.employeeform;
  return { name, phone, shift }
}

export default connect(mapStateToProps, { employeeUpdate, employeeSave, employeeDelete })(EmployeeEdit);

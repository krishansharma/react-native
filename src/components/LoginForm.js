import React, {Component} from 'react';
import {Text} from 'react-native';
import {connect} from 'react-redux';
//import { Container, Item, Input, Header, Body, Content, Title, Button, Text } from 'native-base';
//import { Field,reduxForm } from 'redux-form';
import {emailChanged, passwordChanged, loginUser} from '../actions';
import {Card, CardSection, Input, Button, Spinner} from './common';

class LoginForm extends Component {

  state = { validationError: '', validationOnPass: '' }

//   const validate = values => {
//   const error= {};
//   error.email= '';
//   error.password= '';
//   var ema = values.email;
//   var nm = values.password;
//   if(values.email === undefined){
//     ema = '';
//   }
//   if(values.password === undefined){
//     nm = '';
//   }
//   if(ema.length < 8 && ema !== ''){
//     error.email= 'too short';
//   }
//   if(!ema.includes('@') && ema !== ''){
//     error.email= '@ not included';
//   }
//   if(nm.length > 8){
//     error.password= 'max 8 characters';
//   }
//   return error;
// };

  onEmailChange(text) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      //console.log("Email is Not Correct");
      this.props.emailChanged(text);
      this.setState({ validationError: 'Please Enter Valid email'})
      return false;
    } else {
      this.props.emailChanged(text);
      this.setState({ validationError: ''})
    }

  }

  onPasswordChange(text) {
    let reg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if (reg.test(text) === false) {
      this.props.passwordChanged(text);
      this.setState({ validationOnPass: 'Minimum eight characters, at least one letter and one number:'})
      return false;
    } else {
      this.props.passwordChanged(text);
      this.setState({ validationOnPass: ''})
    }
  }

  onButtonPress() {
    const {email, password} = this.props;

    this.props.loginUser({email, password});
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large"/>;
    }

    return (<Button onPress={this.onButtonPress.bind(this)}>
      Login
    </Button>);
  }

  render() {
    return (<Card>
      <CardSection>
        <Input keyboardType="email-address" secureTextEntry="false" label="Email" placeholder="email@gmail.com" onChangeText={this.onEmailChange.bind(this)} value={this.props.email}/>
      </CardSection>
       <Text style={styles.errorTextStyle}>{this.state.validationError}</Text>
      <CardSection>
        <Input secureTextEntry="secureTextEntry" label="Password" placeholder="password" onChangeText={this.onPasswordChange.bind(this)} value={this.props.password}/>
      </CardSection>
      <Text style={styles.errorTextStyle}>{this.state.validationOnPass}</Text>

      <Text style={styles.errorTextStyle}>
        {this.props.error}
      </Text>

      <CardSection>
        {this.renderButton()}
      </CardSection>
    </Card>);
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};

const mapStateToProps = ({auth}) => {
  const {email, password, error, loading} = auth;

  return {email, password, error, loading};
};

export default connect(mapStateToProps, {emailChanged, passwordChanged, loginUser})(LoginForm);

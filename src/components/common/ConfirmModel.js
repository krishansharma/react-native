import React from 'react';
import { View, Text, Modal } from 'react-native';
import CardSection from './CardSection';
import Button from './Button';

const ConfirmModel = ({children, onAccept, onDecline, visible}) => {
  const { cardSectionStyle, textStyle, contanierStyle } = styles;
  return(
    <View>
    <Modal
        animationType="slide"
        transparent
        visible={visible}
        onRequestClose={() => {}}>
        <View style={contanierStyle}>
          <CardSection style={cardSectionStyle}>
            <Text style={textStyle}>{children}</Text>
          </CardSection>

          <CardSection>
            <Button onPress={onAccept}>Yes</Button>
            <Button onPress={onDecline}>No</Button>
          </CardSection>
        </View>
      </Modal>
      </View>
  )
}

const styles = {
  cardSectionStyle : {
    justifyContent: 'center'
  },

  textStyle : {
    flex: 1,
    fontSize: 18,
    lineHeight: 40,
    textAlign: 'center'
  },

  contanierStyle: {
    justifyContent: 'center',
    flex: 1,
    position: 'relative',
    backgroundColor:'rgba(0,0,0,0.75)'
  }

}

export { ConfirmModel };

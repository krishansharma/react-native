import React from 'react';
import { View, Text, TextInput } from 'react-native';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry, keyboardType }) => {
  const { containerStyle, labelStyle, inputStyle } = styles;
  return (
    <View style={containerStyle}>
     <Text style={labelStyle}>{label}</Text>
     <TextInput
       keyboardType={keyboardType}
       placeholder={placeholder}
       autoCorrect={false}
        style={inputStyle}
        value={value}
        onChangeText={onChangeText}
     />
    </View>
  )
};

const styles = {
  inputStyle: {
    color: '#000',
    paddingLeft: 5,
    paddingRight: 5,
    fontSize: 18,
    lineHeight: 23,
    flex: 2
  },

  labelStyle: {
    flex: 1,
    paddingLeft: 20,
    fontSize: 18
  },

  containerStyle: {
    flex:1,
    height: 40,
    alignItems: 'center',
    flexDirection: 'row'
  }
}

export  { Input };

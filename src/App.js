import React, { Component } from 'react';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import LoginForm from './components/LoginForm';
import Router from './Router';
//import { Field, reduxForm } from 'redux-form';

export default class App extends Component {

 componentWillMount() {
   const config = {
    apiKey: "AIzaSyB7xXNXMs3UMDEvklvKLOv5iL9ksZWR6fQ",
    authDomain: "manager-8e2cf.firebaseapp.com",
    databaseURL: "https://manager-8e2cf.firebaseio.com",
    projectId: "manager-8e2cf",
    storageBucket: "",
    messagingSenderId: "121981999020"
  };
  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  }
 }

 render(){
   const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
  return (
    <Provider store={store}>
      <Router />
    </Provider>
  )
}
}

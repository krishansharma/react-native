import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import EmployeeFormReducer from './EmployeeFormReducer';
import EmployeeReducer from './EmployeeReducer';
//import { reducer as formReducer } from 'redux-form';

export default combineReducers({
  auth: AuthReducer,
  employeeform: EmployeeFormReducer,
  employees: EmployeeReducer,
});
